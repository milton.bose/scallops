
#                                   scallops                                   #

```scala
package scallops
```


--------------------------------------------------------------------------------
                                 Value Members
--------------------------------------------------------------------------------


#### `object BenchmarkTools`                                                  ####

Benchmarking tools


#### `object SlidingOps`                                                      ####

Operations that slides across the DenseVectors



##                             scallops.SlidingOps                             ##

```scala
object SlidingOps
```

Operations that slides across the DenseVectors

--------------------------------------------------------------------------------
                     Value Members From scallops.SlidingOps
--------------------------------------------------------------------------------


##### `def diffBetween[T](xs: DenseVector[T], sep: Int)(implicit arg0: Numeric[T], arg1: Manifest[T], ev: (Int) ⇒ T, conv: CanConvolve[DenseVector[T], DenseVector[T], DenseVector[T]]): DenseVector[T]` #####

Gives difference between two elements separated by the specified seperation. A
seperation of 1 => the adjacent elements

* ___T___ Numeric
* ___xs___ DenseVector of length > 1
* ___sep___ Seperation between the elements. must be > 0 and less than length
* ___returns___ DenseVector of length = xs.length - n

(defined at scallops.SlidingOps)


##### `def movingAvg[T](xs: DenseVector[T], window: Int)(implicit arg0: Numeric[T], arg1: Manifest[T], ev: (Int) ⇒ T, conv: CanConvolve[DenseVector[T], DenseVector[T], DenseVector[T]], opDiv: breeze.linalg.operators.OpDiv.Impl2[DenseVector[T], T, DenseVector[T]]): DenseVector[T]` #####

Computes the moving average. Window size must not be zero.

* ___T___ Numeric
* ___xs___ DenseVector
* ___window___ size of the sliding window > 0. If window > xs.length then window is taken as just xs.length
* ___returns___ DenseVector of length: xs.length - window + 1

(defined at scallops.SlidingOps)


##### `def movingLeastSquareFit[T](xs: DenseVector[T], ys: DenseVector[T], window: Int, keysOfInterest: Option[String] = None)(implicit arg0: Numeric[T], arg1: Manifest[T], ev: (Int) ⇒ T, conv: CanConvolve[DenseVector[T], DenseVector[T], DenseVector[T]], opDiv: breeze.linalg.operators.OpDiv.Impl2[DenseVector[T], T, DenseVector[T]], opDiv2: breeze.linalg.operators.OpDiv.Impl2[DenseVector[T], DenseVector[T], DenseVector[T]], opMult: breeze.linalg.operators.OpMulScalar.Impl2[DenseVector[T], DenseVector[T], DenseVector[T]], opSub: breeze.linalg.operators.OpSub.Impl2[DenseVector[T], DenseVector[T], DenseVector[T]]): Map[String, DenseVector[T]]` #####

Computes Relevent parameters for the Least Square Fitting on a Moving Window

* ___T___ Numeric
* ___xs___ values for independent variable
* ___ys___ values for dependent vaariable
* ___window___ size of the window
* ___keysOfInterest___ least square computation quantities of interest. If None, all keys are returned
* ___returns___ collection of DenseVectors of length: xs.length - window + 1

(defined at scallops.SlidingOps)


##### `def movingSum[T](xs: DenseVector[T], window: Int)(implicit arg0: Numeric[T], arg1: Manifest[T], ev: (Int) ⇒ T, conv: CanConvolve[DenseVector[T], DenseVector[T], DenseVector[T]]): DenseVector[T]` #####

Computes the moving sum. Window size must not be zero.

* ___T___ Numeric
* ___xs___ DenseVector
* ___window___ size of the sliding window > 0. If window > xs.length then window is taken as just xs.length
* ___returns___ DenseVector of length: xs.length - window + 1

(defined at scallops.SlidingOps)



##                           scallops.BenchmarkTools                           ##

```scala
object BenchmarkTools
```

Benchmarking tools


--------------------------------------------------------------------------------
                   Value Members From scallops.BenchmarkTools
--------------------------------------------------------------------------------


##### `def timeIt[T](block: ⇒ T): T`                                           #####

Prints the execution time in micro seconds

* ___T___ return type
* ___block___ code-block to execute
* ___returns___ results of the code execution

(defined at scallops.BenchmarkTools)

