package scallops

import scallops.SlidingOps._
import breeze.linalg.DenseVector
import org.scalacheck.{Arbitrary, Gen}

/**
  * Sliding Operations Tests
  */
class SlidingOpsTest extends UnitTest {



  "diffBy(xs, n)" should "return DenseVector of length (xs.length - n) when xs.length > 1 and 0 < n < xs.length" in {

    val dataGen = for {
      i <- Gen.choose(2, 300)
      xs <- Gen.containerOfN[Array, Double](i, Arbitrary.arbitrary[Double])
      n <-  Gen.choose(1, i - 1)
    } yield (DenseVector(xs), n)

    forAll(dataGen) {(dataGen: (DenseVector[Double], Int)) =>
      val (xs, n) = dataGen
      diffBetween(xs, n).length shouldEqual xs.length - n
    }
  }

  "movingSum(xs, n)" should "return DenseVector for any n > 0" in {

    forAll("xs", "n") { (xs: Array[Float], n: Int) =>
      whenever(n > 0) {
        movingSum(DenseVector(xs), n).length should be >= 0
      }
    }
  }


  "movingAvg(xs, n)" should "return DenseVector for any n > 0 and Double input DenseVector" in {

    forAll("xs", "n") { (xs: Array[Double], n: Int) =>
      whenever(n > 0) {
        movingAvg(DenseVector(xs), n).length should be >= 0
      }
    }
  }

  it should "return DenseVector for any n > 0 and Int input DenseVector" in {

    forAll("xs", "n") { (xs: Array[Int], n: Int) =>
      whenever(n > 0) {
        movingAvg(DenseVector(xs), n).length should be >= 0
      }
    }
  }

  it should "return DenseVector for any n > 0 and Float input DenseVector" in {

    forAll("xs", "n") { (xs: Array[Float], n: Int) =>
      whenever(n > 0) {
        movingAvg(DenseVector(xs), n).length should be >= 0
      }
    }
  }

  "movingLeastSquareFit(xs, ys, n)" should "contain (alpha, beta, SSxx, SSyy, SSxy, xBar, yBar) without filter" in {
    val dataGen = for {
      xs <- Gen.containerOf[Array, Double](Arbitrary.arbitrary[Double])
      ys <- Gen.containerOfN[Array, Double](xs.length, Arbitrary.arbitrary[Double])
      n <-  Gen.choose(1, (xs.length * 2) + 1)
    } yield (DenseVector(xs), DenseVector(ys), n)

    forAll(dataGen) { (dataGen: (DenseVector[Double], DenseVector[Double], Int)) =>
      val (xs, ys, n) = dataGen
      whenever(n > 0) {
        val ls_fit = movingLeastSquareFit(xs, ys, n)
        for(k <- List("alpha", "beta", "SSxx", "SSyy", "SSxy", "xBar", "yBar")) {
          ls_fit should contain key k
        }
      }
    }
  }


}
