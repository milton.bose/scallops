package scallops

import breeze.linalg.{DenseVector, max, min}
import breeze.linalg.operators.{OpDiv, OpMulScalar, OpSub}
import breeze.signal.convolve
import breeze.signal.support.CanConvolve

import scala.math.Numeric

/**
  * Operations that slides across the DenseVectors
  */
object SlidingOps {


  /**
    * Gives difference between two elements separated by the specified seperation.
    * A seperation of 1 => the adjacent elements
    *
    * @param xs DenseVector of length > 1
    * @param sep Seperation between the elements. must be > 0 and less than length
    * @param ev
    * @param conv
    * @tparam T Numeric
    * @return DenseVector of length = xs.length - n
    */
  def diffBetween[@specialized T:Numeric:Manifest] (xs: DenseVector[T], sep: Int)(
    implicit ev: Int => T, conv: CanConvolve[DenseVector[T], DenseVector[T], DenseVector[T]]): DenseVector[T] = {

    require(xs.length > 1, "size of the vector must be > 1")
    require(sep > 0, "seperation must be > 0")
    require(sep < xs.length, "seperation must be < xs.length")

    val ks = new DenseVector[T](sep + 1)
    ks(0) = 1
    ks(-1) = -1
    convolve(xs, ks)
  }


  /**
    * Computes the moving sum. Window size must not be zero.
    *
    * @param xs DenseVector
    * @param window size of the sliding window > 0. If window > xs.length then window is taken as just xs.length
    * @param ev
    * @param conv
    * @tparam T Numeric
    * @return DenseVector of length: xs.length - window + 1
    */
  def movingSum[@specialized T:Numeric:Manifest] (xs: DenseVector[T], window: Int)(
    implicit ev: Int => T, conv: CanConvolve[DenseVector[T], DenseVector[T], DenseVector[T]]): DenseVector[T] = {

    require(window > 0, "window must be > 0")

    if ( xs.length == 0) {
      DenseVector[T]()
    } else {
      val n = min(xs.length, window)
      val ks = DenseVector.fill[T](n)(1)
      convolve(xs, ks)
    }
  }


  /**
    * Computes the moving average. Window size must not be zero.
    *
    * @param xs DenseVector
    * @param window size of the sliding window > 0. If window > xs.length then window is taken as just xs.length
    * @param ev
    * @param conv
    * @tparam T Numeric
    * @return DenseVector of length: xs.length - window + 1
    */
  def movingAvg[@specialized T:Numeric:Manifest] (xs: DenseVector[T], window: Int)(
    implicit ev: Int => T,
    conv: CanConvolve[DenseVector[T], DenseVector[T], DenseVector[T]],
    opDiv: OpDiv.Impl2[DenseVector[T], T, DenseVector[T]]): DenseVector[T] = {

    movingSum(xs, window) / max(min(xs.length, window), 1).asInstanceOf[T]
  }


  /**
    * Computes Relevent parameters for the Least Square Fitting on a Moving Window
    *
    * @param xs values for independent variable
    * @param ys values for dependent vaariable
    * @param window size of the window
    * @param keysOfInterest least square computation quantities of interest. If None, all keys are returned
    * @param ev
    * @param conv
    * @param opDiv
    * @param opDiv2
    * @param opMult
    * @param opSub
    * @tparam T Numeric
    * @return collection of DenseVectors of length: xs.length - window + 1
    */
  def movingLeastSquareFit[@specialized T:Numeric:Manifest] (xs: DenseVector[T],
                                                             ys: DenseVector[T],
                                                             window: Int, keysOfInterest: Option[String] = None
                                                            )(
    implicit ev: Int => T,
    conv: CanConvolve[DenseVector[T], DenseVector[T], DenseVector[T]],
    opDiv: OpDiv.Impl2[DenseVector[T], T, DenseVector[T]],
    opDiv2: OpDiv.Impl2[DenseVector[T], DenseVector[T], DenseVector[T]],
    opMult: OpMulScalar.Impl2[DenseVector[T], DenseVector[T], DenseVector[T]],
    opSub: OpSub.Impl2[DenseVector[T], DenseVector[T], DenseVector[T]]): Map[String, DenseVector[T]] = {

    val SSxx: DenseVector[T] = movingSum(xs :* xs, window)
    val SSyy: DenseVector[T] = movingSum(ys :* ys, window)
    val SSxy: DenseVector[T] = movingSum(xs :* ys, window)
    val xBar: DenseVector[T] = movingAvg(xs, window)
    val yBar: DenseVector[T] = movingAvg(ys, window)

    val beta: DenseVector[T] = SSxy / SSxx
    val alpha: DenseVector[T] = yBar - (beta :* xBar)

    val output = Map("alpha" -> alpha, "beta" -> beta,
    "SSxx" -> SSxx, "SSyy" -> SSyy, "SSxy" -> SSxy, "xBar" -> xBar, "yBar" -> yBar)

    keysOfInterest match {
      case Some(list) => output.filterKeys(list.contains(_))
      case None => output
    }
  }

  def movingLeastSquareFitSeq(ys: DenseVector[Double], window: Int, keysOfInterest: Option[String] = None)(
    implicit ev: Int => Double,
    conv: CanConvolve[DenseVector[Double], DenseVector[Double], DenseVector[Double]],
    opDiv: OpDiv.Impl2[DenseVector[Double], Double, DenseVector[Double]],
    opDiv2: OpDiv.Impl2[DenseVector[Double], DenseVector[Double], DenseVector[Double]],
    opMult: OpMulScalar.Impl2[DenseVector[Double], DenseVector[Double], DenseVector[Double]],
    opSub: OpSub.Impl2[DenseVector[Double], DenseVector[Double], DenseVector[Double]]): Map[String, DenseVector[Double]] = {

    val xs = DenseVector.rangeD(0.0, ys.length.toDouble)
    movingLeastSquareFit(xs, ys, window, keysOfInterest)
  }

}
