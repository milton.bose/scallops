package scallops

/**
  * Benchmarking tools
  *
  */
object BenchmarkTools {

  /**
    * Prints the execution time in micro seconds
    *
    * @param block code-block to execute
    * @tparam T return type
    * @return results of the code execution
    */
  def timeIt[T](block: => T): T = {
    val t0 = System.nanoTime()
    val result = block
    println("Elapsed time: " + (System.nanoTime - t0) / 1000.0 + " micro s")
    result
  }
}
