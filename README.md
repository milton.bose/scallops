# Scala Linear Operations (Scallops)
[![build status](https://gitlab.com/milton.bose/scallops/badges/master/build.svg)](https://gitlab.com/milton.bose/scallops/commits/master)
[![coverage report](https://gitlab.com/milton.bose/scallops/badges/master/coverage.svg)](https://gitlab.com/milton.bose/scallops/commits/master)

## Documentation

Documentation is provided in `api-doc.md`

## Version History

- 0.1: (2016-10-09)
    
    - moving average
    - moving sum
    - difference between two non-adjacent elements
    - linear fit (Least Suquare Fit) parameters

