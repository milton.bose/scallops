name := "scallops"

version := "0.4"

scalaVersion := "2.11.8"

organization := "bose.milton"


libraryDependencies ++= Seq(

  "org.scalanlp"                    %% "breeze"              % "0.12",
  "org.scalanlp"                    %% "breeze-natives"      % "0.12",
  "org.scalanlp"                    %% "breeze-viz"          % "0.12",
  // "com.github.fommil.netlib"         % "all"                 % "1.1.2" pomOnly(),

  // packages required for testing.
  "org.scalactic"                  %% "scalactic"            % "3.0.0",
  "org.scalatest"                  %% "scalatest"            % "3.0.0"       % "test" ,
  "org.scalacheck"                 %% "scalacheck"           % "1.13.2"      % "test",
  "com.lihaoyi"                     % "ammonite"             % "0.7.7"       % "test" cross CrossVersion.full
)


logBuffered in Test := false

initialCommands in (Test, console) := """ammonite.Main().run()"""

scalacOptions in (Compile,doc) ++= Seq("-groups", "-implicits")
